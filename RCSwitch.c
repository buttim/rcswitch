//!make
#include "STC15F2K60S2.h"

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef int BOOL;

#define FOSC 11059200L
#define TRUE (-1)
#define FALSE	0

#define LED_ON	P33=0
#define LED_OFF	P33=1
#define OUT_ON	P35=1
#define OUT_OFF	P35=0

#define LOWER_LIMIT	120			//1200us
#define UPPER_LIMIT	160			//1600us
#define MAX_PULSE_WIDTH	5000	//0.5sec
#define MIN_PULSE_WIDTH	300		//4ms

BOOL status=0,blinking=FALSE,firstRun=TRUE,
	firstRunTooHigh=FALSE;
unsigned t0=0,t1=0,count=0;

void timer0_int (void) __interrupt 1 __using 2 {
	int pin=P32;

	if (++count==20000) count=0;
	//each 'tick' is 10us, so 100ms ON and 100ms OFF
	if (blinking)
		if (count<10000)
			LED_ON;
		else
			LED_OFF;
	
	if (pin==status) {
		if (pin)
			t0++;
		else
			t1++;
	}
	else {
		status=pin;
		if (pin)
			t0=0;
		else {
			if (t0+t1>MAX_PULSE_WIDTH || t0+t1<MIN_PULSE_WIDTH) {
				OUT_OFF;
				blinking=TRUE;
			}
			else {
				if (firstRun && t0>UPPER_LIMIT) {
					blinking=TRUE;
					firstRunTooHigh=TRUE;
				}
				else {
					if (firstRunTooHigh) {
						if (t0<LOWER_LIMIT)
							firstRunTooHigh=FALSE;
					}
					else {
						blinking=FALSE;
						if (t0>UPPER_LIMIT) {
							LED_ON;
							OUT_ON;
						}
						else if (t0<LOWER_LIMIT) {
							LED_OFF;
							OUT_OFF;
						}
					}
					firstRun=FALSE;
				}
			}
			t1=0;
		}
	}
}

void initTimer0() {
	AUXR|=0x80;	//1T mode
	TMOD=0;	//16 bit autoreload timer/counter
	TH0=0xFF;	//about 10us
	TL0=0x91;	//0x10000-0xFF91=111~=10*F_OSC/1e06
	TR0=1;		//run the timer
	ET0=1;		//enable timer interrupt
	EA=1;
}

void main (void) {
	OUT_OFF;
	initTimer0();
	while (1)  {
	}
}


