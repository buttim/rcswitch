# RCSwitch #

Alternative firmware for STC15W204S based RC model electronic switch.

This work is licensed under the [Creative Commons CC BY 4.0 License](https://creativecommons.org/licenses/by/4.0/)